import 'package:flutter/material.dart';
import 'package:shake/shake.dart';
import 'package:timeler/debug.dart';
import 'package:timeler/domain/generator.dart';
import 'package:timeler/domain/model.dart';
import 'package:timeler/domain/time.dart';
import 'package:timeler/domain/update.dart';
import 'package:timeler/resources/strings.dart';
import 'package:timeler/utils.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

class TimelineScreenRoot extends StatefulWidget {
  @override
  _TimelineScreenRootState createState() => _TimelineScreenRootState();
}

class _TimelineScreenRootState extends State<TimelineScreenRoot> {
  MockedTimeUpdater mockedUpdater = MockedTimeUpdater(0);
  StreamTimeUpdater timer;
  DebugVisibility debugOpened = DebugVisibility();
  ShakeDetector shakeDetector;

  final Travel travel = generate(
      "Мордор",
      TravelDescription("", "", ""),
      TimeInterval(0, Duration(hours: 7, minutes: 30).inMilliseconds),
      TimeDuration.fromDuration(Duration(minutes: 20)),
      TimeDuration.fromDuration(Duration(minutes: 40)),
      TimeDuration.fromDuration(Duration(hours: 2)),
      TimeDuration.fromDuration(Duration(minutes: 40)),
      TimeDuration.fromDuration(Duration(minutes: 30)),
      TimeDuration.fromDuration(Duration(minutes: 30)),
      TimeDuration.fromDuration(Duration(hours: 1, minutes: 30)));

  @override
  void initState() {
    super.initState();
    timer = StreamTimeUpdater(mockedUpdater);
    debugOpened.isVisible = false;
    shakeDetector = ShakeDetector.waitForStart(onPhoneShake: () {
      setState(() {
        _toggleDebug();
      });
    });
    shakeDetector.startListening();
  }

  @override
  void dispose() {
    shakeDetector.stopListening();
    super.dispose();
  }

  void _toggleDebug() {
    debugOpened.isVisible = !debugOpened.isVisible;
  }

  @override
  Widget build(BuildContext context) {
    return buildMainWidget();
  }

  Widget buildDebugWidget() {
    return Column(
      children: <Widget>[
        Flexible(child: TimelineScreen(travel, timer)),
        AnimatedContainer(
          height: debugOpened.isVisible ? 100 : 0,
          duration: Duration(milliseconds: 150),
          child: DebugTimeline(debugOpened, mockedUpdater,
              travel.getOverallInterval().getDurationTs()),
        )
      ],
    );
  }

  Widget buildMainWidget() {
    return Stack(children: <Widget>[
      buildDebugWidget(),
      Align(
        child: GestureDetector(
          onTap: () {
            setState(() {
              _toggleDebug();
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(
              Icons.update,
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
          ),
        ),
        alignment: Alignment.bottomRight,
      )
    ]);
  }
}

class TimelineScreen extends StatefulWidget {
  final Travel travel;
  final StreamTimeUpdater timer;

  const TimelineScreen(this.travel, this.timer);

  @override
  _TimelineScreenState createState() => _TimelineScreenState();
}

class _TimelineScreenState extends State<TimelineScreen> {
  @override
  void initState() {
    super.initState();
    widget.timer.start();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(150),
          child: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            flexibleSpace: Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
              child: Center(
                child: StreamBuilder<int>(
                    stream: widget.timer.stream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        Duration d = Duration(
                            milliseconds: widget
                                .travel.steps.last.intervalExpected.tsEnd -
                                snapshot.data);
                        Time time = Time.fromMillis(d.inMilliseconds);
                        bool isFinished = d.inMilliseconds < 0;
                        Color timerColor = Theme
                            .of(context)
                            .primaryColor;
                        return isFinished
                            ? Column(children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 24.0),
                            child: Text(
                              TimerStrings.finished,
                              style: TextStyle(
                                  fontSize: 50, color: timerColor),
                            ),
                          ),
                          FlatButton(
                            color: Theme
                                .of(context)
                                .accentColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Text(
                              TimerStrings.bookNewTickets,
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              launchUrl("https://www.aviasales.ru");
                            },
                          )
                        ])
                            : Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment:
                              CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.alphabetic,
                              children: <Widget>[
                                Text(
                                  time.hours.toString(),
                                  style: TextStyle(
                                      fontSize: 100, color: timerColor),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 0.1),
                                  child: Text(
                                    (d.inMilliseconds % 1000 != 0
                                        ? ":"
                                        : " "),
                                    style: TextStyle(
                                        fontSize: 100, color: timerColor),
                                  ),
                                ),
                                Text(
                                  _formatTwoDigits(time.minutes) + ".",
                                  style: TextStyle(
                                      fontSize: 100, color: timerColor),
                                ),
                                Text(
                                  _formatTwoDigits(time.seconds),
                                  style: TextStyle(
                                      fontSize: 45, color: timerColor),
                                )
                              ],
                            ),
                            Text(
                              TimerStrings.leftTillLeavingCity,
                              style: TextStyle(
                                  fontSize: 20, color: timerColor),
                            )
                          ],
                        );
                      } else {
                        return Text("");
                      }
                    }),
              ),
            ),
          )),
      body: Center(
        child: StreamBuilder<int>(
            stream: widget.timer.stream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Item> items = update(widget.travel, snapshot.data);
                return _buildTimeList(items);
              } else {
                return Text("No data");
              }
            }),
      ),
    );
  }

  Widget _buildTimeList(List<Item> items) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return _buildItem(items[index]);
      },
      itemCount: items.length,
      reverse: true,
    );
  }

  Widget _buildTimeline(List<Item> items) {
    return Timeline.builder(
      itemBuilder: (context, index) {
        return TimelineModel(_buildItem(items[index]),
            position: TimelineItemPosition.right);
      },
      itemCount: items.length,
      position: TimelinePosition.Left,
      reverse: true,
    );
  }

  Widget _buildItem(Item item) {
    return item.state == ItemState.completed
        ? _buildCompletedItemView(item)
        : _buildNormalItemView(item);
  }

  Widget _buildCompletedItemView(Item item) {
    Color color = Colors.black26;
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, right: 24.0),
      child: ListTile(
        leading: Icon(
          Icons.check,
          color: color,
        ),
        title: Text(
          item.text,
          style: TextStyle(fontSize: 18, color: color),
        ),
        trailing: Text(asDayTime(item.interval.tsEnd),
            style: TextStyle(fontSize: 18, color: color)),
      ),
    );
  }

  Widget _buildNormalItemView(Item item) {
    bool isActive = item.state == ItemState.active;
    Color textColor = isActive ? Colors.white : Colors.black;
    Color subtextColor = isActive ? Colors.white60 : Colors.black45;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Card(
          color: isActive ? Theme
              .of(context)
              .accentColor : null,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: InkWell(
            onTap: item.hint != null
                ? () {
              showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return Container(
                        height: 190,
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Center(child: Text(item.hint)),
                        ));
                  });
            }
                : null,
            child: Padding(
              padding: EdgeInsets.only(
                  left: 4.0, top: isActive ? 12 : 4, bottom: isActive ? 12 : 4),
              child: ListTile(
                leading: Padding(
                  padding: EdgeInsets.only(bottom: isActive ? 18.0 : 0),
                  child: Text(
                    item.text,
                    style: TextStyle(
                        fontSize: isActive ? 20 : 16, color: textColor),
                  ),
                ),
                trailing: Padding(
                  padding: EdgeInsets.only(bottom: isActive ? 2.0 : 0.0),
                  child: Column(
                    mainAxisAlignment: isActive
                        ? MainAxisAlignment.end
                        : MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        "в " + asDayTime(item.interval.tsStart),
                        style: TextStyle(color: subtextColor),
                      ),
                      Text(
                        isActive
                            ? "осталось " +
                            _textFormatOfDuration(
                                item.interval.tsEnd - item.currentTs)
                            : _textFormatOfDuration(
                            item.interval.tsEnd - item.interval.tsStart),
                        style: TextStyle(color: subtextColor),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

@immutable
class Time {
  final int hours;
  final int minutes;
  final int millis;
  final int seconds;

  const Time(this.hours, this.minutes, this.seconds, this.millis);

  factory Time.fromMillis(int millisec) {
    int hours = (millisec ~/ 1000) ~/ 3600;
    int minutes = ((millisec ~/ 1000) ~/ 60) - (hours * 60);
    int seconds = (millisec ~/ 1000) - (minutes * 60 + hours * 3600);
    int millis = millisec % 1000;
    return Time(hours, minutes, seconds, millis);
  }
}

String _formatTwoDigits(int value) => value > 9 ? value.toString() : "0$value";

String asDayTime(int millis) {
  DateTime dt = DateTime.fromMicrosecondsSinceEpoch(millis * 1000);
  TimeOfDay tod = TimeOfDay.fromDateTime(dt);
  return tod.hour.toString() + ":" + _formatTwoDigits(tod.minute);
}

String _textFormatOfDuration(int millis) {
  Time timeLeft = Time.fromMillis(millis);
  if (timeLeft.hours > 0) {
    return _formatHoursText(timeLeft.hours);
  }
  if (timeLeft.minutes < 1) {
    return "меньше минуты";
  }
  return _formatMinutesText(timeLeft.minutes);
}

String _formatHoursText(int hours) {
  int check = hours % 10;
  if (check == 1) {
    return hours.toString() + " час";
  } else if (check > 1 && check < 5) {
    return hours.toString() + " часа";
  } else {
    return hours.toString() + " часов";
  }
}

String _formatMinutesText(int minutes) {
  int check = minutes % 10;
  if (check == 1) {
    return minutes.toString() + " минута";
  } else if (check > 1 && check < 5) {
    return minutes.toString() + " минуты";
  } else {
    return minutes.toString() + " минут";
  }
}
