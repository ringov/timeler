import 'package:flutter_test/flutter_test.dart';
import 'package:timeler/domain/generator.dart';
import 'package:timeler/domain/model.dart';

void main() {
  String testName = "test";
  TravelDescription description =
      TravelDescription(testName, testName, testName);

  test("when overall time is shorter than steps duration then can't create",
      () {
    try {
      generate(
        testName,
        description,
        // duration = 29
        TimeInterval(100, 129),
        TimeDuration(5),
        TimeDuration(5),
        TimeDuration(0),
        // limit
        TimeDuration(5),
        TimeDuration(5),
        TimeDuration(5),
        TimeDuration(5), // = 30
      );
      fail("Must throw OverlapTravelException");
    } on OverlapTravelException {}
  });

  test(
      "when overall time equals steps duration, "
      "but buffer limit is too high, then can't create", () {
    try {
      generate(
        testName,
        description,
        // duration = 30
        TimeInterval(100, 130),
        TimeDuration(5),
        TimeDuration(5),
        TimeDuration(1),
        // limit
        TimeDuration(5),
        TimeDuration(5),
        TimeDuration(5),
        TimeDuration(5), // = 30
      );
      fail("Must throw OverlapTravelException");
    } on OverlapTravelException {}
  });

  test(
      "when steps duration is less than overall time "
      "than travel is created with correct timeline", () {
    Travel travel = generate(
      testName,
      description,
      // duration = 15
      TimeInterval(100, 115),
      TimeDuration(1),
      TimeDuration(2),
      TimeDuration(4),
      // limit
      TimeDuration(3),
      TimeDuration(2),
      TimeDuration(1),
      TimeDuration(2), // = 30
    );

    List<Step> steps = travel.steps;

    expect(travel.description.city, description.city);
    expect(travel.description.airportArrival, description.airportArrival);
    expect(travel.description.airportLeaving, description.airportLeaving);

    Phase arrival = steps[0];
    checkPhase(arrival, 100, 100, PhaseType.implicit, PhaseState.expecting,
        CheckpointType.normal);

    Phase airport = steps[1];
    checkPhase(airport, 100, 101, PhaseType.explicit, PhaseState.expecting,
        CheckpointType.normal);

    Phase transportArrival = steps[2];
    checkPhase(transportArrival, 101, 103, PhaseType.explicit,
        PhaseState.expecting, CheckpointType.normal);

    Phase buffer = steps[3];
    checkPhase(buffer, 103, 107, PhaseType.explicit, PhaseState.expecting,
        CheckpointType.normal);

    Phase transportLeaving = steps[4];
    checkPhase(transportLeaving, 107, 110, PhaseType.explicit,
        PhaseState.expecting, CheckpointType.strict);

    Phase passport = steps[5];
    checkPhase(passport, 110, 112, PhaseType.explicit, PhaseState.expecting,
        CheckpointType.strict);

    Phase stuff = steps[6];
    checkPhase(stuff, 112, 113, PhaseType.explicit, PhaseState.expecting,
        CheckpointType.strict);

    Phase gate = steps[7];
    checkPhase(gate, 113, 115, PhaseType.explicit, PhaseState.expecting,
        CheckpointType.strict);
  });
}

void checkCheckpoint(
    Checkpoint checkpoint, int expectedTs, CheckpointType expectedType) {
  expect(checkpoint.type, expectedType);
  expect(checkpoint.checked, false);
  expect(checkpoint.tsExpected, expectedTs);
  expect(checkpoint.tsActual, 0);
}

void checkPhase(Phase phase, int expectedStart, int expectedEnd,
    PhaseType phaseType, PhaseState phaseState, CheckpointType checkpointType) {
  expect(phase.type, phaseType);
  expect(phase.state, phaseState);
  expect(phase.intervalExpected.tsStart, expectedStart);
  expect(phase.intervalExpected.tsEnd, expectedEnd);
  expect(phase.intervalActual.tsStart, 0);
  expect(phase.intervalActual.tsEnd, 0);
  checkCheckpoint(phase.checkpoint, expectedEnd, checkpointType);
}
