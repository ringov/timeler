import 'package:flutter_test/flutter_test.dart';
import 'package:timeler/timeline.dart';

void main() {
  test("correct time", () {
    Time time = Time.fromMillis(20163145);
    expect(time.hours, 5);
    expect(time.minutes, 36);
    expect(time.seconds, 3);
    expect(time.millis, 145);
  });

  test("correct day time", () {
    String s = asDayTime(120000);
    expect(s, "3:02");
  });
}
