import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:timeler/domain/time.dart';

class DebugTimeline extends StatefulWidget {
  final MockedTimeUpdater timeUpdater;
  final DebugVisibility visibility;
  final int endTime;

  const DebugTimeline(this.visibility, this.timeUpdater, this.endTime);

  @override
  _DebugTimelineState createState() => _DebugTimelineState();
}

class DebugVisibility {
  bool isVisible;
}

class _DebugTimelineState extends State<DebugTimeline> {
  double time;

  @override
  void initState() {
    super.initState();
    time = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: widget.visibility.isVisible
            ? Slider(
          value: time,
          onChanged: (value) {
            setState(() {
              time = value;
              widget.timeUpdater.update(time.round());
            });
          },
          min: widget.timeUpdater.initTs.toDouble(),
          max: widget.timeUpdater.initTs.toDouble() +
              widget.endTime.toDouble(),
        )
            : Container(),
      ),
    );
  }
}
