import 'dart:async';

typedef TimeCallback = Function(int ts);

abstract class TimeSource {
  int millis();
}

abstract class TimeUpdater {
  void start();

  void pause();

  void stop();

  void subscribe(TimeCallback callback);
}

class RealTimeUpdater implements TimeUpdater, TimeSource {
  int tick = 500;
  Timer _timer;
  Set<TimeCallback> callbacks = Set();
  int _currentMillis;

  @override
  void start() {
    _timer = Timer.periodic(Duration(milliseconds: tick), (timer) {
      tickTick();
    });
  }

  @override
  void pause() {
    _timer.cancel();
    _timer = null;
  }

  @override
  void stop() {
    if (_timer != null) {
      _timer.cancel();
    }
  }

  @override
  void subscribe(TimeCallback callback) {
    callbacks.add(callback);
  }

  void onTick(int ts, int tickInterval) {}

  @override
  int millis() {
    return DateTime.now().millisecondsSinceEpoch;
  }

  void tickTick() {
    _currentMillis = millis();
    for (TimeCallback c in callbacks) {
      c(_currentMillis);
    }
    onTick(_currentMillis, tick);
  }
}

class MockedTimeUpdater extends RealTimeUpdater {
  int initTs;

  MockedTimeUpdater(int initTs) {
    this.initTs = initTs;
    _currentMillis = this.initTs;
  }

  @override
  void onTick(int ts, int tickInterval) {
    _currentMillis += tickInterval;
  }

  @override
  int millis() {
    return _currentMillis;
  }

  void update(int ts) {
    _currentMillis = ts;
    tickTick();
  }
}

class StreamTimeUpdater implements TimeUpdater {
  StreamController<int> _controller = StreamController.broadcast();
  TimeUpdater timeUpdater;

  Stream<int> get stream => _controller.stream.asBroadcastStream();

  StreamTimeUpdater(this.timeUpdater) {
    this.timeUpdater.subscribe((ts) {
      _controller.sink.add(ts);
    });
  }

  void dispose() {
    _controller.close();
  }

  @override
  void pause() {
    timeUpdater.pause();
  }

  @override
  void start() {
    timeUpdater.start();
  }

  @override
  void stop() {
    timeUpdater.stop();
  }

  @override
  void subscribe(TimeCallback callback) {
    timeUpdater.subscribe(callback);
  }
}
