import 'package:flutter_test/flutter_test.dart';
import 'package:timeler/domain/generator.dart';
import 'package:timeler/domain/model.dart';
import 'package:timeler/domain/update.dart';

void main() {
  String testName = "test";
  TravelDescription description =
      TravelDescription(testName, testName, testName);

  Travel travel = generate(
    testName,
    description,
    // duration = 15
    TimeInterval(100, 115),
    TimeDuration(1),
    TimeDuration(2),
    TimeDuration(4),
    // limit
    TimeDuration(3),
    TimeDuration(2),
    TimeDuration(1),
    TimeDuration(2), // = 30
  );

  test("when ts less than start time then all phase are expecting", () {
    List<Item> items = update(travel, 0);
    checkTypes(items, -1);
  });

  test("switching items states", () {
    List<Item> items = update(travel, 0);
    checkTypes(items, -1);

    items = update(travel, 100);
    checkTypes(items, 1);

    items = update(travel, 101);
    checkTypes(items, 2);
    items = update(travel, 102);
    checkTypes(items, 2);

    items = update(travel, 103);
    checkTypes(items, 3);
    items = update(travel, 106);
    checkTypes(items, 3);

    items = update(travel, 107);
    checkTypes(items, 4);
    items = update(travel, 109);
    checkTypes(items, 4);

    items = update(travel, 110);
    checkTypes(items, 5);
    items = update(travel, 111);
    checkTypes(items, 5);

    items = update(travel, 112);
    checkTypes(items, 6);

    items = update(travel, 113);
    checkTypes(items, 7);
    items = update(travel, 114);
    checkTypes(items, 7);

    items = update(travel, 115);
    checkTypes(items, 8);
  });

  test("when ts greater than end time then all phase are completed", () {
    List<Item> items = update(travel, 115);
    checkTypes(items, items.length);
  });
}

void checkTypes(List<Item> items, int activeIndex) {
  for (int i = 0; i < items.length; i++) {
    if (i < activeIndex) {
      if (items[i].state != ItemState.completed) {
        fail("Item ${items[i].text} must be completed type");
      }
    } else if (i > activeIndex) {
      if (items[i].state != ItemState.expecting) {
        fail("Item ${items[i].text} must be expecting type");
      }
    } else {
      if (items[i].state != ItemState.active) {
        fail("Item ${items[i].text} must be active type");
      }
    }
  }
}
