import 'package:timeler/resources/strings.dart';

import 'model.dart';

int id = 0;

Travel generate(String name,
    TravelDescription description,
    TimeInterval overallInterval,
    TimeDuration airportDuration,
    TimeDuration arrivalTransportDuration,
    TimeDuration bufferLimit,
    TimeDuration leavingTransportDuration,
    TimeDuration passportDuration,
    TimeDuration stuffDuration,
    TimeDuration gateDuration) {
  int stepsDuration = _sumAsMilliseconds([
    airportDuration,
    arrivalTransportDuration,
    leavingTransportDuration,
    passportDuration,
    stuffDuration,
    gateDuration
  ]);
  int expectedBufferDuration = overallInterval.getDurationTs() - stepsDuration;

  if (expectedBufferDuration < bufferLimit.tsDuration) {
    throw OverlapTravelException();
  }

  Phase arrivalFlight = _phase("arrival", overallInterval.tsStart, 0,
      PhaseType.implicit, CheckpointType.normal,
      phaseDescription: TravelStrings.arrivalCheck,
      checkpointDescription: TravelStrings.arrivalCheck);

  Phase airport = _phase("airport", arrivalFlight.checkpoint.tsExpected,
      airportDuration.tsDuration, PhaseType.explicit, CheckpointType.normal,
      phaseDescription: TravelStrings.airportPhase,
      checkpointDescription: TravelStrings.airportCheck,
      hint: StepHintStrings.airportHint);

  Phase transportArrival = _phase(
      "transport_arrival",
      airport.intervalExpected.tsEnd,
      arrivalTransportDuration.tsDuration,
      PhaseType.explicit,
      CheckpointType.normal,
      phaseDescription: TravelStrings.transportArrivalPhase,
      checkpointDescription: TravelStrings.transportArrivalCheck,
      hint: StepHintStrings.arrivalTransportHint);

  Phase gate = _phase("gate", overallInterval.tsEnd - gateDuration.tsDuration,
      gateDuration.tsDuration, PhaseType.explicit, CheckpointType.strict,
      phaseDescription: TravelStrings.gatePhase,
      checkpointDescription: TravelStrings.gateCheck,
      hint: StepHintStrings.gateHint);

  Phase stuff = _phase(
      "stuff",
      gate.intervalExpected.tsStart - stuffDuration.tsDuration,
      stuffDuration.tsDuration,
      PhaseType.explicit,
      CheckpointType.strict,
      phaseDescription: TravelStrings.stuffPhase,
      checkpointDescription: TravelStrings.stuffCheck,
      hint: StepHintStrings.stuffHint);

  Phase passport = _phase(
      "passport",
      stuff.intervalExpected.tsStart - passportDuration.tsDuration,
      passportDuration.tsDuration,
      PhaseType.explicit,
      CheckpointType.strict,
      phaseDescription: TravelStrings.passportPhase,
      checkpointDescription: TravelStrings.passportCheck,
      hint: StepHintStrings.passportHint);

  Phase transportLeaving = _phase(
      "transport_leaving",
      passport.intervalExpected.tsStart - leavingTransportDuration.tsDuration,
      leavingTransportDuration.tsDuration,
      PhaseType.explicit,
      CheckpointType.strict,
      phaseDescription: TravelStrings.transportLeavingPhase,
      checkpointDescription: TravelStrings.transportLeavingCheck,
      hint: StepHintStrings.leavingTransportHint);

  int calculatedBufferDuration = transportLeaving.intervalExpected.tsStart -
      transportArrival.intervalExpected.tsEnd;

  if (expectedBufferDuration != calculatedBufferDuration) {
    throw InconsistentDurationException();
  }

  Phase buffer = _phase("buffer", transportArrival.intervalExpected.tsEnd,
      calculatedBufferDuration, PhaseType.explicit, CheckpointType.normal,
      phaseDescription: TravelStrings.bufferPhase,
      checkpointDescription: TravelStrings.bufferCheck,
      hint: StepHintStrings.bufferHint);

  Travel travel = Travel(_unique("travel"), description, [
    arrivalFlight,
    airport,
    transportArrival,
    buffer,
    transportLeaving,
    passport,
    stuff,
    gate
  ]);

  return travel;
}

class OverlapTravelException implements Exception {}

class InconsistentDurationException implements Exception {}

Checkpoint _checkpoint(String name, int tsExpected, CheckpointType type,
    {String description}) {
  return Checkpoint(_unique(name + "_checkpoint", text: description),
      tsExpected, 0, type, false);
}

Phase _phase(String name, int tsExpected, int duration, PhaseType phaseType,
    CheckpointType checkpointType,
    {String phaseDescription, String checkpointDescription, String hint}) {
  Checkpoint checkpoint = _checkpoint(
      name, tsExpected + duration, checkpointType,
      description: checkpointDescription);
  return Phase(
      _unique(name + "_phase", text: phaseDescription, description: hint),
      TimeInterval.fromDuration(tsExpected, duration),
      TimeInterval(0, 0),
      checkpoint,
      phaseType,
      PhaseState.expecting);
}

int _sumAsMilliseconds(List<TimeDuration> durations) {
  int millis = 0;
  for (TimeDuration d in durations) {
    millis += d.tsDuration;
  }
  return millis;
}

Unique _unique(String name, {String text, String description}) {
  return Unique(_generateId(), name, text: text, description: description);
}

String _generateId() {
  String stringId = id.toString();
  id += 1;
  return stringId;
}
