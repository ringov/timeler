import 'package:flutter/foundation.dart';
import 'package:timeler/domain/model.dart';

enum ItemState { completed, active, expecting }

enum ImportanceType { normal, important }

@immutable
class Item {
  final String id;
  final String text;
  final String hint;
  final TimeInterval interval;
  final ItemState state;
  final ImportanceType importance;
  final bool asCheckpoint;
  final int currentTs;

  const Item(this.id, this.text, this.hint, this.interval, this.state,
      this.importance, this.asCheckpoint, this.currentTs);
}

List<Item> update(Travel travel, int ts) {
  List<Step> steps = travel.steps;
  List<Item> items = List();
  for (Step s in steps) {
    Item item = _map(s, ts);
    items.add(item);
  }
  return items;
}

Item _map(Phase step, ts) {
  ItemState type = _calculateItemState(step.intervalExpected, ts);
  ImportanceType importance = _mapImportance(step.checkpoint.type);
  bool asCheckpoint = step.type == PhaseType.implicit;
  return Item(
      step.unique.id,
      step.unique.text,
      step.unique.description,
      step.intervalExpected,
      type,
      importance,
      asCheckpoint,
      ts);
}

ItemState _calculateItemState(TimeInterval interval, int ts) {
  if (interval.tsStart > ts) {
    return ItemState.expecting;
  } else if (interval.tsEnd <= ts) {
    return ItemState.completed;
  } else {
    return ItemState.active;
  }
}

ImportanceType _mapImportance(CheckpointType type) {
  if (type == CheckpointType.strict) {
    return ImportanceType.important;
  } else {
    return ImportanceType.normal;
  }
}
