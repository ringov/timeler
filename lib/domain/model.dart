import 'package:flutter/foundation.dart';

@immutable
class TimeInterval {
  static TimeInterval fromDuration(int ts, int tsDuration) {
    return TimeInterval(ts, ts + tsDuration);
  }

  final int tsStart;
  final int tsEnd;

  const TimeInterval(this.tsStart, this.tsEnd);

  TimeInterval mutate({int tsStart, int tsEnd}) {
    return TimeInterval(tsStart ?? this.tsStart, tsEnd ?? this.tsEnd);
  }

  int getDurationTs() {
    return tsEnd - tsStart;
  }
}

@immutable
class TimeDuration {
  final int tsDuration;

  const TimeDuration(this.tsDuration);

  static TimeDuration fromDuration(Duration duration) {
    return TimeDuration(duration.inMilliseconds);
  }
}

@immutable
class Unique {
  final String id;
  final String name;
  final String text;
  final String description;

  const Unique(this.id, this.name, {this.text, this.description});
}

@immutable
abstract class Step {
  final Unique unique;

  const Step(this.unique);
}

enum CheckpointType { normal, strict }

@immutable
class Checkpoint extends Step {
  final int tsExpected;
  final int tsActual;
  final CheckpointType type;
  final bool checked;

  const Checkpoint(Unique unique, this.tsExpected, this.tsActual, this.type,
      this.checked)
      : super(unique);

  Checkpoint mutate({int tsExpected, int tsActual, bool checked}) {
    return Checkpoint(this.unique, tsExpected ?? this.tsExpected,
        tsActual ?? this.tsActual, this.type, checked ?? this.checked);
  }
}

enum PhaseState { completed, active, expecting }

enum PhaseType { explicit, implicit }

@immutable
class Phase extends Step {
  final TimeInterval intervalExpected;
  final TimeInterval intervalActual;

  final Checkpoint checkpoint;
  final PhaseType type;
  final PhaseState state;

  const Phase(Unique unique, this.intervalExpected, this.intervalActual,
      this.checkpoint, this.type, this.state)
      : super(unique);

  Phase mutate({TimeInterval intervalExpected,
    TimeInterval intervalActual,
    PhaseState state}) {
    Checkpoint checkpoint;
    if (intervalExpected != null || intervalActual != null) {
      checkpoint = this.checkpoint.mutate(
          tsExpected: intervalExpected.tsEnd, tsActual: intervalActual.tsEnd);
    }

    return Phase(
        this.unique,
        intervalExpected ?? this.intervalExpected,
        intervalActual ?? this.intervalActual,
        checkpoint ?? this.checkpoint,
        this.type,
        state ?? this.state);
  }
}

@immutable
class Travel {
  final Unique unique;
  final TravelDescription description;
  final List<Phase> steps;

  const Travel(this.unique, this.description, this.steps);

  TimeInterval getOverallInterval() {
    return TimeInterval(this.steps.first.intervalExpected.tsStart,
        this.steps.last.intervalExpected.tsEnd);
  }
}

@immutable
class TravelDescription {
  final String city;
  final String airportArrival;
  final String airportLeaving;

  const TravelDescription(this.city, this.airportArrival, this.airportLeaving);
}
