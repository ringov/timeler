import 'package:flutter/material.dart';
import 'package:timeler/timeline.dart';

const Color primaryColor = const Color(0xffFF7518);
const Color accentColor = const Color(0xff495774);

void main() {
  runApp(TimelerApp());
}

class TimelerApp extends StatefulWidget {
  @override
  _TimelerAppState createState() => _TimelerAppState();
}

class _TimelerAppState extends State<TimelerApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Timeler',
        theme: ThemeData(primaryColor: primaryColor, accentColor: accentColor),
        home: TimelineScreenRoot());
  }
}
