class TravelStrings {
  // TODO use TravelDescription for personalizing
  static const String arrivalCheck = "Прилетел";
  static const String airportPhase = "В аэропорту";
  static const String airportCheck = "Вышел из аэропорта";
  static const String transportArrivalPhase = "Еду в город";
  static const String transportArrivalCheck = "Приехал в город";
  static const String bufferPhase = "В городе";
  static const String bufferCheck = "Сесть в транспорт обратно в аэропорт";
  static const String transportLeavingPhase = "Еду обратно в аэропорт";
  static const String transportLeavingCheck = "Доехал до аэропорта";
  static const String passportPhase = "Паспортный контроль";
  static const String passportCheck = "Паспортный контроль";
  static const String stuffPhase = "Прохожу досмотр";
  static const String stuffCheck = "Прошёл досмотр";
  static const String gatePhase = "Жду вылет";
  static const String gateCheck = "Взлетаю!";
}

class StepHintStrings {
  static const String airportHint = '''
  Здесь может быть схема аэропорта и другие прилётные полезности
  
  Под эскалатором около второго гейта есть столовая. 89 пользователей уже поставили ей 5*
  ''';
  static const String arrivalTransportHint = '''
  Как добраться до города, какой в принципе есть транспорт и как его оплачивать
  ''';
  static const String bufferHint = '''
  Простор для фантазии: подборки достопримечательностей, предложения перекусов, дополнительная информация о городе
  Сюда же предложения партнёров
  
  - посмотри башню
  - пообщайся с жителями
  - попробуй местную еду
  ''';
  static const String leavingTransportHint =
  '''
  Как добраться обратно, ссылки на сервисы и предложения
  
  Ой, кажется ты немного опаздываешь. Держи промокод на Uber
  ''';
  static const String passportHint =
  '''
  Советы по паспортному контролю в целом и для конкретного аэропорта
  
  Сотрудник за третьей стойкой говорит по-русски
  ''';
  static const String stuffHint = "Советы по паспортному контролю";
  static const String gateHint =
      "Всё успели!\nУдобное место для интеграций смежных развлекательных сервисов";
}

class TimerStrings {
  static const String leftTillLeavingCity = "До вылета";
  static const String finished = "Взлетаю!";
  static const String bookNewTickets = "Новое путешествие на aviasales";
}

class Notifications {
  static const String canCheckOnlyCurrent =
      "Можно завершить только текущий шаг";
}
